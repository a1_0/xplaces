# README xplaces #

xplaces - web gui for eXtended editing places settings using Flask at back-end and own js-library  

### What is this repository for? ###

* For web-edit parking places;
* For web-edit stadium places;
* etc.

### How do I get set up? ###

```
#!bash

git clone https://1-0@bitbucket.org/a1_0/xplaces.git
```

```
#!bash

cd xplaces
```

```
#!bash

pip install -r requirements.txt --upgrade
```

```
#!bash

sh run.sh
```

 
* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin: 1-0
